[![buddy pipeline](https://app.buddy.works/nelabidiwork/glint-test/pipelines/pipeline/47232/badge.svg?token=75009a78410bc709850b09aae77ca2252ce73daaa32b1dd7bd4d2a6e94c95cbd "buddy pipeline")](https://app.buddy.works/nelabidiwork/glint-test/pipelines/pipeline/47230)
[![Build Status](https://saucelabs.com/buildstatus/nelabidi)](https://saucelabs.com/beta/builds/ad709240ee8442a1ac1a5eb08d831761)
[![Build Status](https://saucelabs.com/browser-matrix/nelabidi.svg)](https://saucelabs.com/beta/builds/ad709240ee8442a1ac1a5eb08d831761)

---
# glint-test #
---
E2E Test for Glint survey webpage using protractor.


# Usage
-----
Chrome browser must be installed on your machine to run this test.

Install protractor:
```
$> npm install protractor -g
```
Update WebDriver server:
```
$> webdriver-manager update
```

Run the test:

```
$> protractor
```