/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.glint;

import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.NgWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author nelabidi
 */
public class WebBrowser implements WebDriver, JavascriptExecutor {

    private final WebDriver _driver;
    private final NgWebDriver _ngDriver;

    private WebBrowser(WebDriver driver) {
        this._driver = driver;
        this._driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        this._ngDriver = new NgWebDriver((JavascriptExecutor) driver);

//        try {
//            URL url = new URL ("http://localhost:4444/wd/hub");
//            WebDriver w= new RemoteWebDriver(url, DesiredCapabilities.chrome());
//            w.get("http://localhost:4444/wd/hub");
//            w.quit();
//        }catch (MalformedURLException | WebDriverException e){
//            System.err.print(e.getMessage());
//        }
    }

    public List<WebElement> getElementsByNgRepeater(String repeater) {

        WebBrowser browser = WebBrowser.getInstance();
        List<WebElement> elems = browser.findElements(ByAngular.repeater(repeater));
        return elems;

    }

    public String getSessionId() {
        return ((RemoteWebDriver) _driver).getSessionId().toString();
    }

    @Override
    public void get(String url) {
        _driver.get(url);
        _ngDriver.waitForAngularRequestsToFinish();
    }

    @Override
    public String getCurrentUrl() {
        _ngDriver.waitForAngularRequestsToFinish();
        return _driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        _ngDriver.waitForAngularRequestsToFinish();
        return _driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
//        _ngDriver.waitForAngularRequestsToFinish();
        return _driver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
//        _ngDriver.waitForAngularRequestsToFinish();
        return _driver.findElement(by);
    }

    @Override
    public String getPageSource() {
        _ngDriver.waitForAngularRequestsToFinish();
        return _driver.getPageSource();
    }

    @Override
    public void close() {
        _driver.close();
    }

    @Override
    public void quit() {
        _driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return _driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return _driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return _driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return _driver.navigate();
    }

    @Override
    public Options manage() {
        return _driver.manage();
    }

    @Override
    public Object executeScript(String script, Object... args) {
        Object o = ((JavascriptExecutor) this._driver).executeScript(script, args);
        _ngDriver.waitForAngularRequestsToFinish();
        return o;
    }

    @Override
    public Object executeAsyncScript(String script, Object... args) {
        Object o = ((JavascriptExecutor) this._driver).executeAsyncScript(script, args);
        _ngDriver.waitForAngularRequestsToFinish();
        return o;

    }

    private static WebBrowser _instance = null;
    private static final Object _syncObject = new Object();
    private static final String USERNAME = "nelabidi";
    private static final String ACCESS_KEY = "555167e9-dacf-40fa-a87c-f80c56fc710a";
    private static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";

    public String getSauceUser() {
        return USERNAME;
    }

    public String getSauceKey() {
        return ACCESS_KEY;
    }

    public static WebBrowser getInstance() {
        if (null == _instance) {
            synchronized (_syncObject) {
                if (_instance == null) {
                    try {
                        DesiredCapabilities caps = DesiredCapabilities.chrome();
                        //caps.setCapability("platform", "Windows XP");
                        //caps.setCapability("version", "43.0");
                        caps.setCapability("name", "glint-test-java");
//                        caps.setCapability("tags", "tag-1");
                        caps.setCapability("build", "maven-build");
                        WebDriver w = new RemoteWebDriver(new URL(URL), caps);
                        //_instance = new WebBrowser(new ChromeDriver());
                        _instance = new WebBrowser(w);

                    } catch (MalformedURLException | WebDriverException e) {
                        System.err.print(e.getMessage());
                    }

                }
            }
        }
        return _instance;
    }

}
