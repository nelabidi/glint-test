/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.glint;

import com.google.gson.Gson;
import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author nelabidi
 */
public final class SurveyPage extends Page {

    public class Question {

        private int index;
        private String questionType;
        private boolean allowsComments;
        private String questionUuid;

        public int getIndex() {
            return index;
        }

        public boolean isRating() {
            return "Rating".equals(questionType);
        }

        public boolean isOpen() {
            return "Open".equals(questionType);
        }

        public boolean allowsComments() {
            return allowsComments;
        }

        public String getUuid() {
            return questionUuid;
        }

        protected Question() {

        }

    }

    private SurveyPage(String url) {
        _url = url;
        navigate();
    }

    public String getUrl() {
        return _url;
    }

    public void navigate() {
        WebBrowser.getInstance().get(this.getUrl());
    }

    private int _currentQuestion = 0;

    public Question getFirstQuestion() {
        return getQuestion(_currentQuestion = 0);
    }

    public Question getNextQuestion() {
        Question q = getQuestion(_currentQuestion + 1);
        if (q != null) {
            _currentQuestion++;
        }
        return q;
    }

    public void answerQuestion(Question q) {
        if (q.isRating()) {
            String script = "arguments[0].click();";
            String css = String.format("#%s-option-%d", q.getUuid(), 2);
            WebElement el = WebBrowser.getInstance().findElement(By.cssSelector(css));
            WebBrowser.getInstance().executeScript(script, el);
        }

    }

    private Question getQuestion(int index) {

        String script = "if($('div#q_" + index + "').length == 0) return 'ERROR';";
        script += " var $scope= angular.element($('div#q_" + index + "')).scope(),q = {};";
        script += "q.index = " + index + ";";
        script += "q.questionType = $scope.question.questionJson.questionType;";
        script += "q.allowsComments= $scope.question.questionJson.allowsComments;";
        script += "q.questionUuid= $scope.question.questionUuid;";
        script += "return JSON.stringify(q);";
        WebBrowser browser = WebBrowser.getInstance();
        String json = (String) browser.executeScript(script);
        if ("ERROR".equals(json)) {
            return null;
        }
        return new Gson().fromJson(json, Question.class);
    }

    private String _url = "http://www.glintinc.com/pulsepreview";

    private static final HashMap<String, SurveyPage> _pageMap = new HashMap<>();

    public static SurveyPage getSurveyPageByUrl(String url) {

        if (!_pageMap.containsKey(url)) {
            _pageMap.put(url, new SurveyPage(url));
        }
        return _pageMap.get(url);

    }
}
