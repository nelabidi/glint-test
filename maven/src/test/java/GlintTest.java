/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.glint.WebBrowser;
import org.junit.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author nelabidi
 */
public class GlintTest extends GlintTestBase {

    @Test(dataProvider = "browserInstance")
    public void TestCase1(WebBrowser browser) {

        System.err.println("Testing browser: ");
        Assert.assertTrue(browser != null);
        browser.get("http://www.google.com");
        System.err.println(browser.getTitle());
        Assert.assertTrue(browser.getTitle().contains("Google"));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        String url = "http://www.glintinc.com/pulsepreview";
//        SurveyPage p = SurveyPage.getSurveyPageByUrl(url);
//        WebBrowser browser = WebBrowser.getInstance();
//
//        System.out.println(SurveyPage.getTitle());
//
//        List<WebElement> elms = browser.getElementsByNgRepeater("question in section.activeQuestions");
//        System.out.println(String.format("Found %d elements", elms.size()));
//
//        SurveyPage.Question q = p.getFirstQuestion();
//        System.out.println(q.getUuid());
//
//        while (q != null) {
//            p.answerQuestion(q);
//            q = p.getNextQuestion();
//
//        }
//
//        browser.quit();
    }

}
