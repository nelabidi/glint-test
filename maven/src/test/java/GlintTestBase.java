
import com.glint.WebBrowser;
import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.testng.SauceOnDemandAuthenticationProvider;
import com.saucelabs.testng.SauceOnDemandTestListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nelabidi
 */
@Listeners({SauceOnDemandTestListener.class})
public class GlintTestBase implements SauceOnDemandSessionIdProvider, SauceOnDemandAuthenticationProvider {

    @BeforeSuite
    public void setupTestSuite() {
        System.out.println("TestBaseClass.setupTestSuite()");
        GlintTestBase.logger.setLevel(Level.ALL);
    }

    @AfterSuite
    public void tearDownTestSuite() {
        System.out.println("TestBaseClass.tearDownTestSuite");
        WebBrowser.getInstance().quit();
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("TestBaseClass.beforeTest()");

    }

    @AfterTest
    public void afterTest() {
        System.out.println("TestBaseClass.afterTest()");

    }

    @DataProvider //(name = "browserInstance", parallel = true) Method testMethod
    public static Object[][] browserInstance() {
        return new Object[][]{{WebBrowser.getInstance()}, {WebBrowser.getInstance()}
    //new Object[]{WebBrowser.getInstance()}//,
    //new Object[]{"firefox", "42", "Linux"},
        };
    }

    protected static final Logger logger = Logger.getLogger(GlintTestBase.class.getName());
    protected static final SauceOnDemandAuthentication auth = new SauceOnDemandAuthentication();
//    protected ThreadLocal<WebBrowser> browser = new ThreadLocal<WebBrowser>();

    @Override
    public String getSessionId() {
        return WebBrowser.getInstance().getSessionId();

    }

    @Override
    public SauceOnDemandAuthentication getAuthentication() {
        auth.setAccessKey(WebBrowser.getInstance().getSauceKey());
        auth.setUsername(WebBrowser.getInstance().getSauceUser());
        return auth;
    }
}
