/////////////////////////////////////////////////////////////////////
//  The SurveyPage object implement methods and properties 
//  of a survey webpage.
//  EXPORTS: SurveyPage, Question
/////////////////////////////////////////////////////////////////////

"use strict"

const PAGE_URL = "http://www.glintinc.com/pulsepreview";

/////////////////////////////
//Question base object 
//////////////////////////////
function Question(props) {
    this.props = props;
}
Question.prototype = {
    get isOpen() {
        return this.props.questionType == 'Open';
    },
    get isRating() {
        return this.props.questionType == 'Rating';
    },
    get allowsComments() {
        return this.props.allowsComments;
    },
    get questionUuid() {
        return this.props.questionUuid;
    },
    get index() {
        return this.props.index;
    }
};

//////////////////////////////////
// SurveyPage object
//////////////////////////////////
module.exports = {
    //return the url for this page
    get url() {
        return PAGE_URL;
    },
    //getFirstQuestion: reset the iterator and return the first question
    getFirstQuestion: function () {
        this._currentQuestion = -1;
        return this._getQuestion();
    },
    getNextQuestion: function () {
        return this._getQuestion();
    },
    //iterator used by getFirstQuestion and getNextQuestion
    _currentQuestion: -1,
    //current question and increment the iterator.
    //NOTE: return null in case if we are at the end of the survey 
    _getQuestion: function () {
        var _self = this;
        return new Promise((resolve, reject) => {
            var index = _self._currentQuestion + 1;
            var script = "if($('div#q_" + index + "').length == 0) return 'ERROR';";
            script += " var $scope= angular.element($('div#q_" + index + "')).scope(),q = {};";
            script += "q.index = " + index + ";";
            script += "q.questionType = $scope.question.questionJson.questionType;";
            script += "q.allowsComments= $scope.question.questionJson.allowsComments;";
            script += "q.questionUuid= $scope.question.questionUuid;";
            script += "return q;"
            browser.driver.executeScript(script).then(c => {
                let q = null;
                if (c !== 'ERROR') {
                    _self._currentQuestion++;
                    q = new Question(c);
                }
                q != null ? resolve(q) : reject(null);
            }, function (e) {
                reject(null);
            });
        });
    },
    //generateRandomAnswerForQuestion
    generateRandomAnswerForQuestion: function (question) {
        if (question.isRating) {
            return this._generateRandomRate();
        } else if (question.isOpen) {
            return "Answer generated " + new Date(Date.now());
        }
    },
    // generateRandomRate: helper function to generate a random rate.
    // returns a value between 0 and 6.
    _generateRandomRate: function () {
        return Math.floor((Math.random() * 7));
    },
    //return the current value of the progress bar
    progressBarValue: function () {
        return new Promise((s, r) => {
            $(".stepCount").getText().then(txt => {
                s(parseInt(txt.substr(0, 1)));
            }).catch(e => r(e));
        });
    },
    //active questions includes answered questions and the current question to be answered 
    //NOTE: return a Promise object
    activeQuestionsCount: function () {
        return new Promise((fulfil, reject) => {
            let locator = by.repeater("question in section.activeQuestions");
            element.all(locator).count().then((c) => {
                fulfil(c);
            }).catch(e => reject(e));
        });
    },
    //set the question answer based on the type of the question
    answerQuestion: function (question, answer) {
        if (question.isRating)
            this.setRatingQuestionRate(question, answer);
        else if (question.isOpen) {
            this.setCommentForQuestion(question, answer);
        }
    },
    //update the given question with the given comment
    setCommentForQuestion: function setCommentForQuestion(question, comment) {
        //Select 'Comment' and 'next' buttons
        //NOTE: the id of the div containing the button is hard-coded 
        let btnComment = $$("div#q_" + question.index + " button");
        //click on the first button, the 'Comment' button
        browser.executeScript("arguments[0].click();", btnComment.get(0).getWebElement());
        //set the comment body
        let txtComment = $("div#q_" + question.index + " textarea");
        txtComment.sendKeys(comment);
        //click save, the last button
        let btnSave = $$("div#q_" + question.index + " .commentActions button");
        browser.executeScript("arguments[0].click();", btnSave.get(1).getWebElement());
        browser.waitForAngular();
    },
    //return the question answer from the UI
    getQuestionAnswer: function (question) {
        if (question.isRating) {
            return this.getRatingQuestionRate(question);
        } else if (question.isOpen) {
            return this.getCommentForQuestion(question);
        }
    },
    //return the comment for the given question as it's shown on the UI
    getCommentForQuestion: function (question) {
        let txtComment = $("div#q_" + question.index + " textarea");
        return txtComment.getAttribute('value');
    },

    //answer a question of type 'Rating'
    setRatingQuestionRate: function (question, rate) {
        let name = question.questionUuid;
        let el = $("#" + name + "-option-" + rate);
        browser.driver.executeScript("arguments[0].click();", el.getWebElement());
        browser.waitForAngular();
    },
    //return the answer of a question of type 'Rating' from the UI
    getRatingQuestionRate: function (question) {
        let name = question.questionUuid;
        let script = "var opts = $('[name=\"" + name + "\"]');";
        script += "var found = -1;";
        script += "for(var i = 0; i < opts.length; i++)";
        script += "if(opts[i].checked) { found = i; break;}";
        script += "return found;";
        return browser.driver.executeScript(script);
    },
    //return a promise resolving to true if the page can submit the survey
    canSubmit: function () {
        let index = this._currentQuestion + 1;
        let css = "section#q_" + index + " button[type='submit']";
        return new Promise(function (resolve, reject) {
            //check if button Submit response is displayed
            browser.findElement(by.css(css)).then(function (elm) {
                elm.isDisplayed().then(resolve, reject);
            }, function () {
                resolve(false);
            });
        });
    },
    //submit 
    submit: function () {
        let index = this._currentQuestion + 1;
        let css = "section#q_" + index + " button[type='submit']";
        return new Promise(function (resolve, reject) {
            //check if button Submit response is displayed
            browser.findElement(by.css(css)).then(function (elm) {
                elm.click().then(function () {
                    resolve(true);
                }, function () {
                    resolve(false);
                });
            }, function () {
                resolve(false);
            });
        });
    },
    //navigate the Survey Page URL and reset the Question Iterator
    navigate: function navigate() {
        browser.get(this.url);
        //FIXME: check for expired session
        browser.refresh();
        browser.waitForAngular();
        //refreshing the page re-initialize the page state
        this._currentQuestion = -1;

    }

};