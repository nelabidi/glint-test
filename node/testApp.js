/**
 *  testApp.js: implement a facad for the glint test framework.
 * 
 */

"use strict"
//set jasmine timeout here.
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

//import pages 
const SurveyPage = require("./surveyPage");

//helper functions 
function getCurrentPageTitle() {

    return browser.getTitle();
}

//exports object
module.exports.SurveyPage = SurveyPage;
module.exports.getCurrentPageTitle = getCurrentPageTitle;