const USE_SERVER = process.env.USE_SERVER;

var Config = {
  framework: 'jasmine',
  specs: ['surveyPageSpec.js']
}

//choose selenium server based on USE_SERVER env variable
if (USE_SERVER) {
  if (USE_SERVER ==="sauce") {
    Config.sauceUser = "nelabidi";
    Config.sauceKey = "555167e9-dacf-40fa-a87c-f80c56fc710a";
    Config.sauceBuild = "node-build";
    Config.multiCapabilities = [{
      browserName: 'chrome',
      name: 'node-chrome'
    }, {
      browserName: 'firefox',
      name: 'node-firefox'
    }];
  }else if(USE_SERVER === "local"){
    Config.seleniumAddress = 'http://localhost:4444/wd/hub';
  }else if (USE_SERVER === "buddy")
  {
    Config.multiCapabilities = [{
      browserName: 'chrome',
      seleniumAddress: 'http://selenium-ch:4444/wd/hub',
      name: 'node-chrome'
    }/*
    * looks like firefox is broken
    , {
      browserName: 'firefox',
      seleniumAddress: 'http://selenium-ff:4444/wd/hub',
      name: 'node-firefox'}*/
    ];

  }
}

exports.config = Config;