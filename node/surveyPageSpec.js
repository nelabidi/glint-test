///////////////////////////////////////////////////////////////////////
// The purpose of this test is to check for :
//
//  1. Ability to answer all questions     
//  2. Check that progress bar updates as we answer the questions
//  3. Check adding comments to questions functionality  
//  4. Check the ability to submit the responses and to be redirected to "Thank You" page 
//
///////////////////////////////////////////////////////////////////////


const TestApp = require("./testApp");
const SurveyPage = TestApp.SurveyPage;


describe("Glint survey page", function () {

    beforeAll(function () {
        SurveyPage.navigate();

    });
    //self test 
    it("check if the current page is a survey page", function () {
        expect(TestApp.getCurrentPageTitle()).toMatch("Questionnaire");
    });

    //check if the UI is correctly initialized.
    it("progress bar should be 0 at the start", function () {
        expect(SurveyPage.progressBarValue()).toEqual(0);
    });

    it("should have at least one active question to be answered", function () {
        expect(SurveyPage.activeQuestionsCount()).toEqual(1);
    });

    it("can answer all questions and checks for the progress bar updates", function () {

        let surveyQuestionsCount = 0;

        function finalCheck() {
            //check that all questions on the page are visited
            expect(SurveyPage.activeQuestionsCount()).toEqual(surveyQuestionsCount);
            //check the progress bar updates
            expect(SurveyPage.progressBarValue()).toEqual(surveyQuestionsCount);
        }
        let answerQuestion = function (question) {
            surveyQuestionsCount++;
            let answer = SurveyPage.generateRandomAnswerForQuestion(question);
            SurveyPage.answerQuestion(question, answer);
            expect(SurveyPage.getQuestionAnswer(question)).toEqual(answer);
            expect(SurveyPage.progressBarValue()).toEqual(surveyQuestionsCount);

            SurveyPage.getNextQuestion().then(answerQuestion, finalCheck);
        };

        SurveyPage.getFirstQuestion().then(answerQuestion);
    });

    it("can add comments to questions that allows comments", function () {
        var count = 0;
        let comment = "Comment for question ";

        let addComment = function (question) {
            count++;
            if (question.allowsComments) {
                SurveyPage.setCommentForQuestion(question, comment + count);
                let savedComment = SurveyPage.getCommentForQuestion(question);
                expect(savedComment).toEqual(comment + count);
            }
            SurveyPage.getNextQuestion().then(addComment, function () {});
        };
        SurveyPage.getFirstQuestion().then(addComment);
    });

    it("can submit the responses and be redirected to 'Thank You' page", function () {

        let checkAndSubmit = function () {
            expect(SurveyPage.canSubmit()).toBe(true);
            expect(SurveyPage.submit()).toBe(true);
            expect(TestApp.getCurrentPageTitle()).toMatch("Thank You");
        }
        let getNext = function (question) {
            let answer = SurveyPage.generateRandomAnswerForQuestion(question);
            SurveyPage.answerQuestion(question, answer);
            SurveyPage.getNextQuestion().then(getNext, checkAndSubmit);
        };
        SurveyPage.getFirstQuestion().then(getNext);
    });



});